//
//  PVDownloader_Utils.m
//  Protovate
//
//  Created by Beny Boariu on 25/11/13.
//  Copyright (c) 2013 Protovate LLC. All rights reserved.
//

#import <CommonCrypto/CommonDigest.h>
#import "PVDownloader_Utils.h"
#import "PVDownloader_Constants.h"

@implementation PVDownloader_Utils

/**
 **     Create a Files folder in Library/Application Support
 **/
+ (void)createFilesDirectory
{
    NSString *strFolderPath         = [self getFilesFolderPath];
    NSError *error;
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:strFolderPath])
    {
        [[NSFileManager defaultManager] createDirectoryAtPath:strFolderPath withIntermediateDirectories:YES attributes:nil error:&error];
        
        NSURL *pathURL= [NSURL fileURLWithPath:strFolderPath];
        [self addSkipBackupAttributeToItemAtURL:pathURL];
    }
}

/**
 **     Remove the JSON folder in Library/Application Support
 **/
+ (void)removeJSONDirectory
{
    NSString *strFolderPath         = [self getFilesFolderPath];
    
    NSFileManager *defaultManager   = [NSFileManager defaultManager];
    
    if ([defaultManager fileExistsAtPath:strFolderPath])
    {
        NSError *error = nil;
        [defaultManager removeItemAtPath:strFolderPath error:&error];
        
        if (error)
        {
            //PVLog(@"Error while removing Images folder");
        }
    }
}

/**
 **     Return File folder path in Library/Application Support
 **/
+ (NSString *)getFilesFolderPath
{
    NSArray *paths                  = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    NSString *libraryDirectory      = [paths objectAtIndex:0];
    libraryDirectory                = [libraryDirectory stringByAppendingPathComponent:kApplicationSupportDirectory];
    NSString *strFolderPath         = [libraryDirectory stringByAppendingPathComponent:kFilesDirectory];
    
    return strFolderPath;
}

/**
 **     Remove File located at @strFilePath
 **/
+ (BOOL)removeFileAtPath:(NSString *)strFilePath
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    return [fileManager removeItemAtPath:strFilePath error:nil];
}

/**
 **     Write a bunch of data into a file
 **/
+ (BOOL)writeData:(NSData *)data toFileWithName:(NSString *)strFileName
{
    NSString *strFolderPath         = [self getFilesFolderPath];
    NSString *strFilePath           = [strFolderPath stringByAppendingPathComponent:strFileName];
    
    BOOL didWrite                   = [data writeToFile:strFilePath atomically:YES];
    
    return didWrite;
}

/**
 **     Exclude item from being backed up in iCloud
 **/
+ (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    assert([[NSFileManager defaultManager] fileExistsAtPath:[URL path]]);
    
    NSError *error = nil;
    BOOL success = [URL setResourceValue:[NSNumber numberWithBool: YES]
                                  forKey:NSURLIsExcludedFromBackupKey
                                   error:&error];
    if(!success)
    {
        //PVLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
    else
    {
        //PVLog(@"success in excluding %@ from backup", [URL lastPathComponent]);
    }
    
    return success;
}

/**
 **     Retrieve the md5 hash of a string
 **/
+ (NSString *)getMD5StringFromString:(NSString *)strString
{
    const char *cstr = [strString UTF8String];
    unsigned char result[16];
    CC_MD5(cstr, strlen(cstr), result);
    
    return [NSString stringWithFormat:
            @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}



@end
