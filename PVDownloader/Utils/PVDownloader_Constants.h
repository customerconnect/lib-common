//
//  PVDownloader_Constants.h
//  Protovate
//
//  Created by Beny Boariu on 25/11/13.
//  Copyright (c) 2013 Protovate LLC. All rights reserved.
//

#ifndef PVDownloader_Constants_h
#define PVDownloader_Constants_h

#define k_MinCache                                      300
#define k_MaxCache                                      1*24*60*60

//>     Files folder will be inside Library/Application Support
#define kApplicationSupportDirectory                    @"/Application Support"
#define kFilesDirectory                                 @"/Files"

//>     Key to save last download date in UserDefaults
#define k_NSUserDefaults_LastJSONDownloadDate           @"Last JSON Download Date"

//>     Key to save last download date in UserDefaults
#define k_NSUserDefaults_LastFileDownloadDate           @"Last File Download Date"

#endif
