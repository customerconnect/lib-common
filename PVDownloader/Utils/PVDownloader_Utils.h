//
//  PVDownloader_Utils.h
//  Protovate
//
//  Created by Beny Boariu on 25/11/13.
//  Copyright (c) 2013 Protovate LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PVDownloader_Utils : NSObject

/**
 **     Create a Files folder in Library/Application Support
 **/
+ (void)createFilesDirectory;

/**
 **     Retrieve the md5 hash of a string
 **/
+ (NSString *)getMD5StringFromString:(NSString *)strString;

/**
 **     Return JSON folder path in Library/Application Support
 **/
+ (NSString *)getFilesFolderPath;

/**
 **     Remove File located at @strFilePath
 **/
+ (BOOL)removeFileAtPath:(NSString *)strFilePath;

/**
 **     Write a bunch of data into a file
 **/
+ (BOOL)writeData:(NSData *)data toFileWithName:(NSString *)strFileName;

@end
