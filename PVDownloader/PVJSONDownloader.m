//
//  PVJSONDownloader.m
//  Protovate
//
//  Created by Beny Boariu on 23/11/13.
//  Copyright (c) 2013 Protovate LLC. All rights reserved.
//

#import "PVJSONDownloader.h"
#import "PVDownloader_Utils.h"
#import "PVDownloader_Constants.h"

@implementation PVJSONDownloader

- (id)init
{
    return [self initWithMinCacheTime:0 maxCacheTime:0 andDelegate:nil];
}

- (id)initWithMinCacheTime:(NSTimeInterval)tiMinCache maxCacheTime:(NSTimeInterval)tiMaxCache andDelegate:(id)delegate
{
    self = [super init];
    
    if (self)
    {
        //>     Monitor connection status
        [[AFNetworkReachabilityManager sharedManager] startMonitoring];
        
        if (tiMinCache)
        {
            self.tiMaxCache     = tiMinCache;
        }
        else
        {
            self.tiMinCache     = k_MinCache;
        }
        
        if (tiMaxCache)
        {
            self.tiMaxCache     = tiMaxCache;
        }
        else
        {
            self.tiMaxCache     = k_MaxCache;
        }
        
        if (delegate)
        {
            self.delegate       = delegate;
        }
        
        [PVDownloader_Utils createFilesDirectory];
    }
    
    return self;
}

- (void)downloadJSON:(NSString *)strURL
{
    //>     File is saved locally, by md5 string obtained from url
    NSString *strFileName       = [PVDownloader_Utils getMD5StringFromString:strURL];
    NSString *strFilePath       = [NSString stringWithFormat:@"%@/%@.json", [PVDownloader_Utils getFilesFolderPath], strFileName];
    
    BOOL oldJSONExists          = [[NSFileManager defaultManager] fileExistsAtPath:strFilePath isDirectory:NO];
    if (oldJSONExists)
    {
        NSString *strKey                = [NSString stringWithFormat:@"%@_%@", strFileName, k_NSUserDefaults_LastJSONDownloadDate];
        NSDate *dateLastDownload        = [[NSUserDefaults standardUserDefaults] objectForKey:strKey];
        NSTimeInterval tiTimePassed     = [[NSDate date] timeIntervalSinceDate:dateLastDownload];
        
        NSError *error;
        NSString *strCachedJSON         = [NSString stringWithContentsOfFile:strFilePath encoding:NSUTF8StringEncoding error:&error];
        if (tiTimePassed < self.tiMinCache)
        {
            //>     Return the cached json
            if ([self.delegate respondsToSelector:@selector(jsonDownloadFinished:)])
            {
                [self.delegate jsonDownloadFinished:strCachedJSON];
            }
        }
        else
            if ([self connected])
            {
                //>     Internet connection available -> update cached json
                [self performDownload:strURL];
            }
            else
                if (tiTimePassed < self.tiMaxCache)
                {
                    //>     No connection to update, so return cached version
                    if ([self.delegate respondsToSelector:@selector(jsonDownloadFinished:)])
                    {
                        [self.delegate jsonDownloadFinished:strCachedJSON];
                    }
                }
                else
                {
                    //>     No connection, and cached version too old, return error
                    if ([self.delegate respondsToSelector:@selector(jsonDownloadFailed:)])
                    {
                        [self.delegate jsonDownloadFailed:@"Connect to internet to download a more recent version."];
                    }
                }
    }
    else
    {
        [self performDownload:strURL];
    }
}

- (void)performDownload:(NSString *)strURL
{
    AFHTTPSessionManager *sessionManager    = [[AFHTTPSessionManager alloc] init];
    sessionManager.responseSerializer       = [AFHTTPResponseSerializer serializer];
    
    [sessionManager GET:strURL
              parameters:nil
                 success:^(NSURLSessionDataTask *task, id responseObject) {
                     
                     //NSString *responseString = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                     //NSLog(@"Response: %@", responseString);
                     
                     NSString *strFileName      = [PVDownloader_Utils getMD5StringFromString:strURL];
                     NSString *strKey           = [NSString stringWithFormat:@"%@_%@", strFileName, k_NSUserDefaults_LastJSONDownloadDate];
                     
                     strFileName                = [NSString stringWithFormat:@"%@.json", strFileName];
                     BOOL didWriteData          = [PVDownloader_Utils writeData:responseObject toFileWithName:strFileName];
                     if (didWriteData)
                     {
                         [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:strKey];
                         [[NSUserDefaults standardUserDefaults] synchronize];
                         
                         if ([self.delegate respondsToSelector:@selector(jsonDownloadFinished:)])
                         {
                             [self.delegate jsonDownloadFinished:nil];
                         }
                     }
                     else
                     {
                         if ([self.delegate respondsToSelector:@selector(jsonDownloadFailed:)])
                         {
                             [self.delegate jsonDownloadFailed:@"Error while saving JSON locally"];
                         }
                     }
                 }
                 failure:^(NSURLSessionDataTask *task, NSError *error) {
                    
                     if ([self.delegate respondsToSelector:@selector(jsonDownloadFailed:)])
                     {
                         [self.delegate jsonDownloadFailed:@"Error while downloading JSON"];
                     }
                 }];
}

- (BOOL)connected
{
    BOOL isReachable = [AFNetworkReachabilityManager sharedManager].reachable;
    return isReachable;
}

@end
