//
//  PVJSONDownloader.h
//  Protovate
//
//  Created by Beny Boariu on 23/11/13.
//  Copyright (c) 2013 Protovate LLC. All rights reserved.
//

/**
 **     This class requires AFNetworking 2.0 lib to be included in project,
 **     as it uses AFHTTPSessionManager to deal with json requests.
 **
 **
 **     How to use:
 **
 **     PVJSONDownloader *pvJSONDownloader = [[PVJSONDownloader alloc] init];
 **     [pvJSONDownloader downloadJSON:@"http://www.json-generator.com/j/cvDFMGsiyG?indent=4"];
 **/

#import <Foundation/Foundation.h>
#import "AFHTTPSessionManager.h"

@protocol PVJSONDownloaderDelegate <NSObject>
@optional

- (void)jsonDownloadFinished:(NSString *)strJSON;
- (void)jsonDownloadFailed:(NSString *)strErrorMessage;

@end

@interface PVJSONDownloader : NSObject

//>     Delegate to notify caller when download finished
@property (nonatomic, weak) id<PVJSONDownloaderDelegate> delegate;

@property (nonatomic, assign) NSTimeInterval tiMinCache;
@property (nonatomic, assign) NSTimeInterval tiMaxCache;

- (id)initWithMinCacheTime:(NSTimeInterval)tiMinCache maxCacheTime:(NSTimeInterval)tiMaxCache andDelegate:(id)delegate;
- (void)downloadJSON:(NSString *)strURL;

@end
