//
//  PVFileDownloader.h
//  Protovate
//
//  Created by Beny Boariu on 28/11/13.
//  Copyright (c) 2013 Protovate LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AFHTTPSessionManager.h"

@protocol PVFileDownloaderDelegate <NSObject>
@optional

- (void)fileDownloadFinished:(NSString *)strFilePath hash:(NSString *)strHash;
- (void)fileDownloadFailed:(NSError *)error;
- (void)fileDownloadProgress:(float)fProgress;

@end

@interface PVFileDownloader : NSObject

//>     Delegate to notify caller when download finished
@property (nonatomic, weak) id<PVFileDownloaderDelegate> delegate;

//>     Flag used to show that a file is already being download in this object
@property (nonatomic, assign, readonly) BOOL isDownloadingFile;

- (void)downloadFile:(NSString *)strFilename type:(NSString *)strType hash:(NSString *)strNewHash url:(NSString *)strURL;

@end
