//
//  PVFileDownloader.m
//  Protovate
//
//  Created by Beny Boariu on 28/11/13.
//  Copyright (c) 2013 Protovate LLC. All rights reserved.
//

#import "PVFileDownloader.h"
#import "AFHTTPRequestOperation.h"
#import "PVDownloader_Utils.h"
#import "PVDownloader_Constants.h"

@interface PVFileDownloader ()

@property (nonatomic, assign, readwrite) BOOL isDownloadingFile;

@end

@implementation PVFileDownloader

- (id)init
{
    self = [super init];
    
    if (self)
    {
        [PVDownloader_Utils createFilesDirectory];
    }
    
    return self;
}

- (void)downloadFile:(NSString *)strFilename type:(NSString *)strType hash:(NSString *)strNewHash url:(NSString *)strURL
{
    NSString *strOldHash                = [[NSUserDefaults standardUserDefaults] objectForKey:strFilename];
    
    //>     Check if file was previously saved on the disk
    if (strOldHash)
    {
        //>     File was already saved, check if it's still valid
        if ([strOldHash isEqualToString:strNewHash] )
        {
            NSString *strFilePath   = [NSString stringWithFormat:@"%@/%@.%@", [PVDownloader_Utils getFilesFolderPath], strFilename, strType];
            
            if ([[NSFileManager defaultManager] fileExistsAtPath:strFilePath])
            {
                //>     File already exists and is up to date
                if ([self.delegate respondsToSelector:@selector(fileDownloadFinished:hash:)])
                {
                    
                    [self.delegate fileDownloadFinished:strFilePath hash:strNewHash];
                }
            }
            else
            {
                //>     Hash exists, but no file is saved locally, so download file
                [self performDownloadFromURL:strURL filename:strFilename type:strType hash:strNewHash];
            }
        }
        else
        {
            //>     An update exists for this file
            [self performDownloadFromURL:strURL filename:strFilename type:strType hash:strNewHash];
        }
    }
    else
    {
        //>     File was never downlaoded, so download it now
        [self performDownloadFromURL:strURL filename:strFilename type:strType hash:strNewHash];
    }
}

- (void)performDownloadFromURL:(NSString *)strURL filename:(NSString *)strFileName type:(NSString *)strType hash:(NSString *)strHash
{
    self.isDownloadingFile              = YES;
    
    NSURL *url                          = [NSURL URLWithString:strURL];
    NSURLRequest *request               = [NSURLRequest requestWithURL:url];
    
    AFHTTPRequestOperation *operation   = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    
    NSString *strFilePath               = [NSString stringWithFormat:@"%@/%@.%@", [PVDownloader_Utils getFilesFolderPath], strFileName, strType];
    
    [operation setOutputStream:[NSOutputStream outputStreamToFileAtPath:strFilePath append:NO]];
    
    [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
        
        float fProgress                 = (float)totalBytesRead / totalBytesExpectedToRead;
        if ([self.delegate respondsToSelector:@selector(fileDownloadProgress:)])
        {
            [self.delegate fileDownloadProgress:fProgress];
        }
    }];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [[NSUserDefaults standardUserDefaults] setObject:strHash forKey:strFileName];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        self.isDownloadingFile = NO;
        
        if ([self.delegate respondsToSelector:@selector(fileDownloadFinished:hash:)])
        {
            [self.delegate fileDownloadFinished:strFilePath hash:strHash];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        //>     Remove file created by output stream
        [PVDownloader_Utils removeFileAtPath:strFilePath];
        
        self.isDownloadingFile = NO;
        
        if ([self.delegate respondsToSelector:@selector(fileDownloadFailed:)])
        {
            [self.delegate fileDownloadFailed:error];
        }
    }];
    
    [operation start];
}

@end
