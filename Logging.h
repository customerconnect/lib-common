/*
**  Protovate Common Library - Code for debug logging
*/

#if (DEBUG)

#define PVLog( s, ... ) NSLog( @"<%p %@:(%d)> %@", self, [[NSString stringWithUTF8String:__FILE__] lastPathComponent], __LINE__, [NSString stringWithFormat:(s), ##__VA_ARGS__] )

#else

#define PVLog( s, ... ) ;

#endif
